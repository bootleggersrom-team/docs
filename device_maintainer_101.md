
Bootleggers Team Guide #??: Adding a new official device
========================================================

Now that you got in here, congrats! Thank you once again for taking your time and supporting our ROM and help us reach a whole new community.

We hope you're ready for the steps that takes being a device maintainer not just for this ROM, but for every ROM.

If this is your first experience with Custom ROMs maintainership, every project has their own way to handle things. This is ours, it might seem a bit clunky at first, but it works.

Shishufied Builds and what it means
-----------------------------------

Shishufied is our way to say official. It's the way people get to know that the ROM is being taken care by somebody we trust. Now, if you're an official maintainer and you're ready to do a release, we need you to do the following:

On your device tree,on your `bootleg_device.mk` you will need to add the following lines:

    BOOTLEGGERS_BUILD_TYPE := Shishufied
    DEVICE_MAINTAINERS := "Your maintainer name"
    

> Note: _This is still a Work in Progress, this current implementation can change soon. Please, ask the current source managers about this._

**IMPORTANT NOTICE:** We do NOT advice you to build and release ONLY a build with gapps. If you want to release our ROM, by default we suggest that you do **Vanilla** builds. If for some reason you want to do a **GApps** release, you have to do both. We prefer a bit of freedom of choice, not everybody likes Google apps.

Now that everything is ready, you just have to build and wait for your Shishufied zip to be done.

Release ready to be shipped!
----------------------------

Once that your build gets done, you'll get a message like this:

            ▄ █ ███████▄▄      ▐██████████████     ████                ▄▄███████▀▄
     ████████ █ ██████████▄    ▐██████████████▌    ████             ▄██████████████
    ▐█▌▄▄▄▄▄▄ █ ████  ▐███▌          ████          ████            ▄████▀        ▀
         ▄███ █ ██████████▄          ████          ████           ▐████
     ▄██▀▀▐██ █ ████████████▄        ████          ████           ▐███▌       ██████
       ▄▄█▀ █ █ ████     ████        ████          ████           ▐████       ▀▀▀███
     ▀▀  ▄▄█▀ █ ████    ▄████        ████          ████            ▀████▄       ▐███
    ▄▄██▀▀    █ ████████████▀        ████          ████████████     ▀███████████████
         ▐▀█▀ █ ████████▀▀▀          ████          ████████████        ▀▀███████▀▀
    
              Bootleggers ROM         
              #KeepTheBootleg        
    
    
    
    The build is done, be sure to get it on:
    /dainocore/bootleg/out/target/product/chime/BootleggersROM-Tirimbino4chime.7.0-Stable-Shishufied-Vanilla-20230206-120133.zip
    And your build size is: 1.1GB (1233586343)
    
    Also, enjoy your Shishufied build
    

Now, there's some important info in that message that we need you to get in handy becuase you'll need it for the following step.

If you focus on the build size message line, you'll find that the size that in non-shishufied builds shows up in MB, but now it's just a bunch of numbers. Why is that? Well, that's because now the build size is in bytes, something that we need for our API.

This API is being used to run our Main Group chat bot, our OTA app and well, you can also use it in any way you want. You can see the api working in [here](https://bootleggersrom-devices.github.io/api/devices.json).

For this, we've made a crusty easy method to make our maintainers upload/update their builds on our API. It's not automatic but it's a way to make sure we control who is really updating their devices. For this, you'll need to modify two files on our API repo a.k.a [our devices website repo](https://github.com/BootleggersROM-Devices/BootleggersROM-Devices.github.io) (if these files doesn't exist, create them). You have to clone our devices website repo and add the following files:

**FILE 1: \_devices/\[device\_codename\].md**

    ---
    # Device and Maintainer Info
    codename: [your device codename]
    fullname: [your device full name]
    maintainer: [your maintainer name]
    xdathread: [optional, your XDA Thread link]
    # Build Info
    filename: [your build filename]
    buildsize: [the build size in bytes]
    mirrorlink: [optional, your build link mirror]
    # Gapps build info
    gapps_filename: [your build filename]
    gapps_buildsize: [the build size in bytes]
    gapps_mirrorlink: [optional, your build link mirror]
    ---
    

**FILE 2: \_changelog/\[device\_codename\].md**

    ---
    codename:[device_codename]
    ---
    
    Devices:
    
    - [Some of your device specific changes]
    
    Bootleggers:
    
    - [Some of our source changes]
    

Now, this might sound a bit confusing maybe, but we'll give you an example to make this more reasonable. Now, let's say ElDainosor is going to officially release Bootleggers for _chime_, in that case, he has to create a file in the _\_devices_ folder and another one in the _\_changelog_ folder called `chime.mk`. Then the contents inside these files will be something like this:

**FILE 1: \_devices/chime.md**

    ---
    codename: chime
    fullname: Poco M3/Redmi 9T
    maintainer: ElDainosor
    # xdathread: [optional, your XDA Thread link]
    filename: BootleggersROM-Tirimbino4chime.7.0-Stable-Shishufied-Vanilla-20230206-120133.zip
    buildsize: 1233586343
    # mirrorlink: [optional, your build link mirror]
    ---

> Note: _In this case, I don't have a XDA Thread for my builds, and I don't have any way to mirror my builds. So, I've commented these sections out. I can remove them too, for a cleaner file. That's up to you to do the way you want. If you are going to use any of these variables in the future, you can leave them commented out until you update them. If you don't, then you can delete them too._

**FILE 2: \_changelog/chime.md**

    ---
    codename:chime
    ---
    
    Devices:
    
    - Added another kernel
    - Device updated to current trees
    
    Bootleggers:
    
    - Initial 7.0 Release
    - Added three finger screenshot gesture
    

The changes are done, it's time to make a commit and push it to the devices repo. The API will upgrade after 5 minutes you push the update. After that, you can link everything to your favorite telegram channel or whatever source you want to tell your community you're released a new build. And that's it! That's how you update your device. Then, once the files are done, it's a matter to update things like the `filename`, `buildsize` and `changelog` accordingly to the new release you want to push.

Hope this guide helped you to understand how our way to release builds works!

If you have any questions, please, ask the current source managers and they'll help you out!
